var streamifier = require('streamifier');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Schema = mongoose.Schema;
var Grid = require('gridfs-stream');

var Q = require('q');
var _ = require('lodash');

mongoose.connect('mongodb://localhost/mmb');
Grid.mongo = mongoose.mongo;
var conn = mongoose.connection;
var GridFS = Grid(conn.db);

//Handle DB connection
function dbConnection(database) {
    mongoose.connect(database.server.url, database.server.options);
    var conn = mongoose.connection;

    conn.on('error', function(err) {
        mongoLogger.error('Error while connecting to mongo database: ' + err);
        process.exit(1);
    });

    conn.once('open', function() {
        mongoLogger.info('Connected to mongodb: ' + database.server.url);
        GridFS = Grid(conn.db, mongoose.mongo);
    });
    return conn;
} 

var imageSchema = new Schema({
    imageId: Schema.Types.ObjectId
});

var Image = mongoose.model('Image', imageSchema);

function saveFile(file, filename, mimetype) {
    var deferred = Q.defer();

    var objectId = ObjectId();
    var writestream = GridFS.createWriteStream({
        _id: objectId,
        filename: filename,
        mode: 'w',
        content_type: mimetype
    });

    file.pipe(writestream);

    writestream.on('finish', function (file) {
        deferred.resolve(objectId.toString());
    });

    writestream.on('error', function (error) {
        deferred.reject(error);
    });

    return deferred.promise;
}


function retrieveFile(fileId) {
    return GridFS.createReadStream({
        _id: ObjectId(fileId)
    });

}


module.exports.dbConnection = dbConnection;
module.exports.saveFile = saveFile;
module.exports.retrieveFile = retrieveFile;

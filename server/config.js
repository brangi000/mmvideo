var _ = require('lodash');
var config = require('../config.json');
var configLocations = ['/etc/mmb/config.json', './config.json'];

var overrideConfig;
_.each(configLocations, function(configLocation) {
    try {
        overrideConfig = require(configLocation);
        config = _.extend(config, overrideConfig);
    } catch (err) {
        if (err.code !== 'MODULE_NOT_FOUND') {
            throw err;
        }
    }
});

module.exports = config;

var express    = require('express');        
var app        = express();                 
var bodyParser = require('body-parser');
db = require('./db.js');
var openTok = require('./openTok.js');
var imageSaver = require('./imageSaver.js');
var Busboy = require('busboy');
var _ = require('lodash');
var multiparty = require('multiparty');
var util = require('util');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        

var router = express.Router();              


router.use(function(req, res, next) {
    console.log('Something is happening.');
    next(); // go to routes
});

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});


// Updload image
// Form:
//     auctionid
router.route('/image')

	.post(function(req, res) {
             var form = new multiparty.Form();
             var auctionId;
                 form.parse(req, function(err, fields, files) {
                     auctionId = JSON.stringify(fields.auctionId[0]);
                     console.log("Post with request: " + JSON.stringify(fields.auctionId[0]));
                        
                 });

	    var busboy = new Busboy({ headers : req.headers });

            busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
            
                imageSaver.saveFile(file, filename, mimetype).then(function(objectId){
                console.log("Post with request with name" + filename);
                res.writeHead(200, {'content-type': 'application/json'});
                res.end(JSON.stringify({'imgId': objectId}));
            
            }).fail(function(error) {
                console.log("Error Saving Image: " + error);
                return next(error(420, error));
            }).done();

        });
            busboy.on('finish', function() {
        });

        req.pipe(busboy);             

		
	});

// Retreive Image from collection
router.route('/image/:image_id')

    .get(function(req, res) {
            var imgStream = imageSaver.retrieveFile(req.params.image_id);

            res.set('Content-Type', "image/jpeg");

            imgStream.on("error", function (err) {
                console.log("Got error while processing stream " + err.message);
                return next(error(422, err));
            });

            imgStream.on("end", function () {
                console.log('Retrieved image file.');
            });

            imgStream.pipe(res);
    });
// Create session ID
router.route('/videosid')
	.post(function(req, res) {
		var userId = req.body.user;
                console.log('Create sid for user ' + userId);
                openTok.createSession(userId);
                res.json({ message: 'opentokSession created!'});
		
	});

// Generate token for session ID
router.route('/videotok')
        .post(function(req, res) {
            var userId = req.body.user;
            console.log('Generate sessionToken for user' + userId);
            var sessionId =  req.body.sid;
            console.log('With sessionid ' + sessionId);
            var token = openTok.generateToken(sessionId);
            res.json({ sessionToken: token, expiration: '60'});
	});

// Root api
app.use('/api', router);

app.listen(port);
console.log('MarketMob api running on port ' + port);

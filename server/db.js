var logger = require('winston');
var Riak = require('basho-riak-client');
var async = require('async');
 
var cluster;
var client;

function connect() {
    var nodes = [];
    var addrs = [ [ '146.185.156.173',8087 ]];
    addrs.forEach(function (a) {
        var host = a[0];
        var port = a[1];
        var node = new Riak.Node({ remoteAddress: host, remotePort: port, cork: false  });
        nodes.push(node);
    });
    
    cluster = new Riak.Cluster({ nodes: nodes });
    client = new Riak.Client(cluster);
    client.ping(function (err, rslt) {
        logger.info('[connected to riak and ping]', rslt);
    });

    return client;
}


function saveSessionId(user, sid){
    
    var client = connect();
    var id = 'opentokSessionId';

    var riakObj = new Riak.Commands.KV.RiakObject();
    riakObj.setContentType('text/plain');
    riakObj.setValue(sid);
    client.storeValue({
            bucket: user, key: id,
            value: riakObj
    }, function (err, rslt) {
            if (err) {
                throw new Error(err);
            }
    });

    //cluster.stop();
    client_shutdown();

}

function saveImageId(auctionId, imageKey, imageId){
    
    var client = connect();
    var id = imageKey;

    var riakObj = new Riak.Commands.KV.RiakObject();
    riakObj.setContentType('text/plain');
    riakObj.setValue(imageId);
    client.storeValue({
            bucket: auctionId, key: id,
            value: riakObj
    }, function (err, rslt) {
            if (err) {
                throw new Error(err);
            }
    });

    //cluster.stop();
    client_shutdown();


}

function client_shutdown() {
    client.shutdown(function (state) {
        if (state === Riak.Cluster.State.SHUTDOWN) {
            process.exit();
        }
    });
}

function saveSessionIddd(user){
    var client = connect();
    var id = 'opentokSessionId';

    var opentokIds =  [{
        id: sid,
    }];

    var storeFuncs = [];
    opentokIds.forEach(function (sessionId) {
    storeFuncs.push(function (async_cb) {
        client.storeValue({
                bucket: user,
                key: id,
                value: sessionId
            },
            function(err, rslt) {
                    async_cb(err, rslt);
                }
            );
        });
    });
    
    async.parallel(storeFuncs, function (err, rslts) {
        if (err) {
            throw new Error(err);
        }
    });

}


function updateSessionId(user){

    var sid;

    sid = updateSessionId(user);

    if(sid !=''){
    
    
    }
    
    
    riakObj.setValue(bashoman);
    client.storeValue({ value: riakObj }, function (err, rslt) {
    if (err) {
            throw new Error(err);
        }
    });

}


function updateSessionId(user, targetSid){
    var id = 'opentokSessionId';
    var sid;
    client = connect();
    client.fetchValue({ bucket: user, key: id, convertToJs: true },
    function (err, rslt) {
        if (err) {
            throw new Error(err);
        } else {
                var riakObj = rslt.values.shift();

                if(typeof riakObj != 'undefined'){
                        sid = riakObj.value
                        logger.info("SessionId Found", sid);

                 } else {
                        logger.info("Not found");
                        sid = '';
                 }
            }
        }
    );
    client_shutdown();
    return sid
}

module.exports.saveImageId =saveImageId;
module.exports.saveSessionId = saveSessionId;

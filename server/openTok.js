
var OpenTok = require('opentok');
var Q = require('q');
var db = require('./db.js');

function createSession(userId) {

    var opentok = new OpenTok('45385992', '06d7e73bf3f9b014d16dd8a92599d9522d896f69');
    var session;
    opentok.createSession({
        mediaMode: "routed"
    }, function(error, result) {
        if (error) {
            console.log("Error creating session: " + error);
        } else {
            session = result;
            console.log("SessionId: " + JSON.stringify(session.sessionId));
            db.saveSessionId(userId, JSON.stringify(session.sessionId));
        }
    });

}

function generateToken(sessionId) {
    var opentok = new OpenTok('45385992', '06d7e73bf3f9b014d16dd8a92599d9522d896f69');
    var token = opentok.generateToken(sessionId, {
        role: 'publisher',
        expireTime: (new Date().getTime() / 1000) + (60 * 60) // in 1 hr
    });
    return token;
}

module.exports.createSession = createSession;
module.exports.generateToken = generateToken;


